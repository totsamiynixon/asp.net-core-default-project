"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Human = (function () {
    function Human(id, name) {
        this.id = id;
        this.name = name;
    }
    return Human;
}());
exports.Human = Human;
//# sourceMappingURL=Human.js.map